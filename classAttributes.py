class InfoGen:
    """ A class to generate the info of students of a college. """

    college = "MIET"
    collegeCode = 68
        
    def __init__(self, name, roll):
        self.name = name
        self.roll = roll
        print("Object is initialized.")
        print("Student's college is:", InfoGen.college)

student01 = InfoGen("Anupam", 40)
print(student01.name)
print(student01.roll)
print(InfoGen.collegeCode)

student02 = InfoGen("Vipul", 10)
print(student02.name)
print(student02.roll)
print(student02.__class__.collegeCode)
