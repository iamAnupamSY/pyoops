class Circle():
    """ A class to get the area of a circle. """
    pi = 3.14

    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return self.radius * self.radius * Circle.pi

radius01 = Circle(2)
result = radius01.area()
print("Circle area:", result)
