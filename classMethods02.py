class Circle():
    """ A class to get the area of a circle. """
    pi = 3.14

    def __init__(self, radius=2):
        self.radius = radius

    def area(self):
        return self.radius * self.radius * Circle.pi

    def setRadius(self, radius):
        self.radius = radius

radius01 = Circle()
radius01.setRadius(5)
result = radius01.area()
print("Circle area:", result)
