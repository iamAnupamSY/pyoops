class Sample():
    """ A sample class to test the ways of method calling. """

    def __init__(self, name):
        self.name = name

    def getName(self):
        print("Name is:", self.name)

obj01 = Sample("Anupam")

# First way of method calling : bounded method call
obj01.getName()

# Second way of method calling : unbounded method call
Sample.getName(obj01)
