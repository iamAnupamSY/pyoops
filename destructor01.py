class Vehicle:
    def __init__(self):
        print("Constructor called...A Vehicle object is created.")
    def __del__(self):
        print("Destructor called...A Vehicle object is deleted.")

car = Vehicle()
del car
    
