class Vehicle:
    """ A class to implement constructor and destructor. """
    def __init__(self):
        print("Constructor called...A Vehicle object is created.")
    def __del__(self):
        print("Destructor called...A Vehicle object is deleted.")

car = Vehicle()
bus = car
del car

# NOTE: In the above stage, destructor didn't call, as reference count is not zero.

del bus   
# NOTE: In the above stage, destructor called, as reference count is zero.
