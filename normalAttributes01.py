class Sample:
    def __init__(self, name, age):
        self.name = name
        self.age = age

def main():
    # Instantiating the class with first instance 
    obj01 = Sample("Anupam", 22)
    print("First attribute value:", obj01.name)
    print("Second attribute value:", obj01.age)

    # Instantiating the class with second instance
    obj02 = Sample("Sandeep", 35)
    print("First attribute value:", obj02.name)
    print("Second attribute value:", obj02.age)

if __name__ == "__main__":
    main()
