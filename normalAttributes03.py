class Sample:
    def __init__(self):
        self.name = input("Enter the name:")
        self.age = int(input("Enter the age:"))

    def show_data(self):
        print("Your name is:", self.name)
        print("Your age is:", self.age)

def objectsDataDisplay(totalInstances, instanceList):
    print("[OUTPUT]: Printing the objects data")
    count = 0
    while(count < totalInstances):
        instanceList[count].show_data()
        print("Instance dictionary containing all its attributes:", instanceList[count].__dict__)
        count = count + 1  

def main():
    totalInstances = int(input("Enter the total no of instances:"))
    print("[INPUT]: Taking the objects data")
    instanceList = []
    count = 0
    while(count < totalInstances):
        instanceList.append(Sample())
        count = count + 1

    # calling the objectsDataDisplay function.
    objectsDataDisplay(totalInstances, instanceList)

if __name__ == "__main__":
    main()
