class InfoGen:
    """ A class to generate the info of students of a college. """

    def __init__(self, name, roll):
        self.name = name
        self.roll = roll
        print("Object is initialized.")

student01 = InfoGen("Anupam", 40)
print(student01.name)
print(student01.roll)

student01.age = 22
student01.branch = "CSE"
print(student01.age)
print(student01.branch)

student02 = InfoGen("Vipul", 10)
print(student02.name)
print(student02.roll)
