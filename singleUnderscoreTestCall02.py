from test import *
from test import _complexFun

# Calling the simple function of imported module.
simpleFun()

# Calling the function with single underscore of imported module.
_complexFun()
